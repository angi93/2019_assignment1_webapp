import pytest

@pytest.fixture
def client():
    from webapp import app
    app.app.config['TESTING'] = True
    return app.app.test_client()
