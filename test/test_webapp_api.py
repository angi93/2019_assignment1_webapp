from urllib.parse import urlencode
import json

def call(client, path):
    response = client.get(path)
    try:
        resp = json.loads(response.data.decode('utf-8'))
        return resp
    except ValueError:
        return {'error': 'Request error!!!'}
                                                

def test_milan(client):
    result = call(client, '/milan')
    assert result[0]['city'] == 'Milan'

def test_london(client):
    result = call(client, '/london')
    assert result[0]['city'] == 'London'

