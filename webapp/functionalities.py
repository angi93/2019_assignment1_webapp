from influxdb import InfluxDBClient
import pyowm
import datetime


def saveData(data):
    # Open connection to InfluxDB
    client = InfluxDBClient(host='localhost', port=8086)
    # Select the DB
    client.switch_database('weatherdb')
    # Create istance to weather API
    owm = pyowm.OWM('fd627c7324ddaaf42ef3f53a0799b644')  # You MUST provide a valid API key
    # Get actual date and time in ISO format
    i = datetime.datetime.now()
    
    # Search for current weather in Milan
    observation = owm.weather_at_place(data['city'])
    w = observation.get_weather()
    
    # Weather details
    temperature = w.get_temperature('celsius')
    wind = w.get_wind()
    humidity = w.get_humidity()
    json_data = [{
                    "measurement": "Temperature",
                    "tags": {
                                "user": data['user']
                    },
                    "city": data['city'],
                    "time": i.isoformat(),
                    "fields": {
                                "temp": temperature['temp'],
                                "temp_max": temperature['temp_max'],
                                "temp_min": temperature['temp_min'],
                                "humidity": humidity
                    }
                }]
    
    client.write_points(json_data)
    return json_data

