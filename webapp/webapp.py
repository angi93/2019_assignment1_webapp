from flask import request, Flask
import json
import functionalities

app = Flask(__name__)


@app.route('/milan')
def milan():
    return json.dumps(functionalities.saveData({"city": "Milan", "user": "user112"}))

@app.route('/london')
def london():
    return json.dumps(functionalities.saveData({"city": "London", "user": "user112"}))

@app.route('/bangkok')
def bangkok():
    return json.dumps(functionalities.saveData({"city": "Bangkok", "user": "user112"}))

if __name__ == "__main__":
    app.run(threaded=True)